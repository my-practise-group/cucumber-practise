$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("features/UC001Login.feature");
formatter.feature({
  "line": 2,
  "name": "Testing Login requirement feature",
  "description": "As a client administrator\r\nI want to login to the application\r\nSo I can manage a user account",
  "id": "testing-login-requirement-feature",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@regression"
    }
  ]
});
formatter.before({
  "duration": 615800,
  "status": "passed"
});
formatter.before({
  "duration": 6753801300,
  "status": "passed"
});
formatter.before({
  "duration": 5016200,
  "status": "passed"
});
formatter.before({
  "duration": 163500,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Login test of Business Console application",
  "description": "",
  "id": "testing-login-requirement-feature;login-test-of-business-console-application",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 7,
      "name": "@regression"
    },
    {
      "line": 7,
      "name": "@wip"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "I login with \"client username\" \u0026 \"client password\"",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should be able to see the text \"login message\"",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.iAmOnTheLoginPage()"
});
formatter.result({
  "duration": 114775700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "client username",
      "offset": 14
    },
    {
      "val": "client password",
      "offset": 34
    }
  ],
  "location": "LoginSteps.i_login_with(String,String)"
});
formatter.result({
  "duration": 2529793500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "login message",
      "offset": 34
    }
  ],
  "location": "LoginSteps.iShouldBeAbleToSeeTheText(String)"
});
formatter.result({
  "duration": 72600,
  "status": "passed"
});
formatter.after({
  "duration": 107200,
  "status": "passed"
});
formatter.after({
  "duration": 66400,
  "status": "passed"
});
formatter.after({
  "duration": 67500,
  "status": "passed"
});
});