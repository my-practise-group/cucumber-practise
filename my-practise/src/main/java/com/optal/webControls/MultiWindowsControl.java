package com.optal.webControls;

import org.openqa.selenium.WebDriver;

/**
 * Created by Edwin on 21/01/2018.
 */
public class MultiWindowsControl extends BaseControl{


    public static void switchToNewWindowPage(WebDriver driver){
        String currentHandle = driver.getWindowHandle();
        for(String winHandle : driver.getWindowHandles()){
            if(!winHandle.equalsIgnoreCase(currentHandle)){
                driver.switchTo().window(winHandle);
                return;
            }
        }
    }
}
