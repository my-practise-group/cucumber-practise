package com.optal.pages;

import com.optal.webControls.DropDownControl;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RequestNewCardPage  extends BasePage{

    @FindBy(id = "cardProfile")
    private WebElement cardProfile = null;

    @FindBy(id = "currencies")
    private WebElement currencies = null;

    public RequestNewCardPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void fillNewCardRequest(List<List<String>> data) {
        String singleCard = data.get(1).get(1);
        DropDownControl.selectDropDownByVisibleText(cardProfile, singleCard);
        DropDownControl.selectDropDownByVisibleText(currencies, data.get(2).get(1));
    }
}
