package com.optal.steps;

import com.optal.utilities.TestData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.optal.pages.BasePage;
import com.optal.pages.LandingPage;
import com.optal.support.WorldHelper;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


public class LoginSteps {

    private WorldHelper helper;
    private BasePage basePage;
    private LandingPage landingPage;

    public LoginSteps(WorldHelper helper) {
        this.helper = helper;
    }


    @Given("^I am on the login page$")
    public void iAmOnTheLoginPage() {
        basePage = helper.getBasePage();
    }

    @When("^I login with \"([^\"]*)\" & \"([^\"]*)\"$")
    public void i_login_with(String username, String password)  {
        username = TestData.getValue("client username");
        password = TestData.getValue("client password");
        landingPage = helper.getBasePage().loginWith(username,password);
    }

    @Then("^I should be able to see the text \"([^\"]*)\"$")
    public void iShouldBeAbleToSeeTheText(String loginMessage) {
        loginMessage = TestData.getValue(loginMessage);
        assertThat(landingPage.validateLogin(loginMessage), is(true));
    }
}
